\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}


Le problème du Budget Participatifs (\textit{Participatory Budgeting} abrégé PB) correspond à l'organisation d'un vote pour déterminer l'allocation du budget d'une entité.
La ville de Paris a par exemple alloué un budget de 100 millions d'euros annuellement pour ce genre de votes, Porto Alegre au Brésil en dépense 200 dans lequel on dénombre 55000 participants.
\newline
Cependant, il existe un certain nombre de problématiques liés à ce genre de vote. Si celui-ci sélectionne les projets ayant obtenu le plus grand nombre de votes alors les projets concernant les espaces les plus peuplés seront systématiquement sélectionnés.
Des associations peuvent se coordonner pour déstabiliser le votes. Enfin le budget doit être suffisamment consommé \textit{i.e.} on doit minimiser la fraction restante du budget après le vote.
\newline
Nous retenons plusieurs notions de ces problématiques. Celle de l'absence de coalition : une fraction de votant.es répliquent leurs bulletins pour optimiser leurs gains même si ces bulletins ne correspondent pas à leurs intentions.
La représentativité : une fraction de votant.es sous-représenté dû à une ou plusieurs caractéristiques (\textit{e.g.} arrondissement faiblement peuplé ). Des problématiques computationnelles : les algorithmes déterminant le vainqueur doit s'exécuter en temps \enquote{raisonnable}.
Enfin l'absence de stratégies gagnante (\textit{strategyproofness}) pour un individu en falsifiant son bulletin.
La \textit{strategyproofness} (voir \ref{def:strategyproof}) permet d'assurer l'absence de coalition, mais aussi de ne pas sélectionner des projets inutiles
issus d'une stratégie ce sera l'objet de notre étude. %Durant l'année 2022, des étudiant.es de l'ENS ont schématisé des projets pour la ville de Lyon. Certains portent sur l'amélioration de l'urbanisme, d'autres sur la construction de laboratoire junior,
%des introductions à la recherche pour des lycéens. La ville ne peut subventionner qu'un sous ensemble de ces projets et décide donc d'organiser un vote.
%\newline 

Pour déterminer les mécanismes \textit{strategyproof} nous dégageons plusieurs types de vote. Tout d'abord, les bulletins peuvent prendre différentes formes. Par exemple nous pouvons ordonner nos préférences, approuver plusieurs projets
ou même un seul. L'organisateur du vote peut maximiser le \enquote{contentement} individuel ou globale (nous reviendrons plus tard sur cette notion de \enquote{contentement}).
Cependant, les mécanismes dont les bulletins décrivent plus finement les intentions des votant.es (\textit{e.g.} en ordonnant ses préférences ou en attribuant un score aux objets) ne sont pas pour une grande part \textit{strategyproof}.
En l'état, les résultats d'impossibilités \cite{MINE:Gibbard73} ont conduit à la recherche de propriété plus faible.
Notamment à celle de l'équité d'un vote \cite{DBLP:conf/nips/PierczynskiSP21} ou la détection de coalition
\cite{DBLP:series/synthesis/2011Chalkiadakis}. Cette étude a pour motivation l'extension des travaux effectués sur la \textit{strategyproofness} de \textit{Participatory Budgeting}.
Notamment de l'utilisation de l'algorithme du sac-à-dos pour déterminer l'issu d'un vote.
Nous traitons des mécanismes unitaires (approuver qu'un seul projet), et nous généralisons les mécanismes de budgets participatifs dans le cadre
de bulletins binaire (approuver un sous-ensemble de projets) afin de mettre en place des mécanismes révélant les liens entre les projets via l'introduction des matrices
de score hors du cadre des résultats d'impossibilités énoncés par \textbf{Gibbard}.


\subsection{Différents Mécanismes de votes}

On souhaite catégoriser les différentes formes de vote. Comme dit précédemment les bulletins des votant.es peuvent prendre plusieurs formes :
\begin{itemize}
    \item \textit{Binaire} : Nous validons où non un certain nombre de projets, lorsque le nombre de projets à sélectionner est de
          1 nous parlerons de bulletin \tc{unitaire}.
    \item \textit{Ordonné} : Nous ordonnons les projets par ordre de préférence
    \item \textit{Fonctionnel} : Nous attribuons à chaque projet une certaine valeur dans \(\mathbb{R}\).
\end{itemize}

La notion introduite précédemment \enquote{contentement} est appelée plus communément \tc{utilité}. L'utilité d'un votant peut prendre plusieurs formes, on en distinguera deux pour les bulletins binaires.
L'utilité d'\tc{approbation} vaut la somme des projets sélectionnés par le vote contenu dans le bulletin du votant. L'utilité \tc{pondérée} correspondant à la somme des valeurs des objets sélectionnés
par le vote parmi ceux approuvé dans le bulletin du votant.

Distinguons maintenant distinguer deux types de mécanismes :
\begin{itemize}
    \item \textit{utilitaire} : Un mécanisme de vote est dit \tc{utilitaire} s'il maximise la somme des utilités des votant.es. Plus généralement
          nous pouvons maximiser une combinaison linéaire des utilités des votant.es.
          \label{def:util}
    \item \textit{non-utilitaire} : Pour les autres mécanismes (\textit{e.g.} \enquote{$\max\min$} qui maximise l'utilité minimale de chacun des votant.es).
\end{itemize}

\subsection{Exemple et Applications}

Pour clarifier toutes ces notions prenons un exemple :
\begin{exmp}
    \label{ex:pb}
    Nous nous donnons trois votant.es, \(\mathcal{N} = \{1,2,3\}\). Ils peuvent voter pour quatre projets :
    \begin{itemize}
        \item \textbf{P1}: Construire un métro
        \item \textbf{P2}: Ouvrir une nouvelle ligne de bus
        \item \textbf{P3}: Installer un local à vélo
        \item \textbf{P4}: Installer une poubelle
    \end{itemize}
    Une organisation indépendante choisit quels projets  sont sélectionnés en fonction des déclarations \linebreak de chaque votant.
    Ces projets (\tc{objets}) coûtent respectivement 30, 20, 25 et 5 de la monnaie locale. Dans ce monde notre budget (\tc{contrainte}) est de 55. On donne la possibilité aux votant.es
    de voter pour au plus 2 projets et on leur pose demande les projets qu'ils préfèrent \linebreak (\tc{déclaration}). Chaque votant.e cherche à maximiser son utilité (\textit{e.g.} le nombre de projets choisis parmi ses préférences).
    Muni de ce cadre, nous pouvons imaginer plusieurs algorithmes \linebreak (\tc{fonction de choix social}) : maximiser la somme des utilités (\tc{utilitaires}) ou d'autres plus exotiques (\tc{non-utilitaires}).
    \\
    Nous supposons que les votes des votant.es 1 et 2 sont répartis de la sorte :
    \[
        \begin{tabular}[ht]{||c|c|c|c||}
            \hline
            P1 & P2 & P3 & P4 \\
            \hline
            2  & 0  & 2  & 0  \\
            \hline
        \end{tabular}
    \]
    Le votant 3 préfère P1 et P2 mais il sait que si l’organisation maximise les votes des votant.es, le projet 2 ne sera pas sélectionné s'il déclare ses vraies préférences.
    \(f(d) = \{1,3\}\) où \(d\) dénote les déclarations sans mensonges. Le votant 3 déclare alors préférer 2 et 4, ce qui donne \(f(d^*) = \{1,2,4\}\).
\end{exmp}

L'objectif de l’organisation est d'inciter les votant.es à dire la vérité dans leur déclaration. Leur seul moyen d'action
étant l'algorithme de sélection des objets.

\subsection{Glossaire}

Nous pouvons désormais introduire les notations que nous allons utiliser tout le long de cette étude:
\begin{itemize}
    \item[-] \(\mathcal{N}\): Un ensemble de \tc{votant.es}.
    \item[-] \(\mathcal{B}\): La \tc{contrainte} de votes e.g le nombre de candidats élus, le budget d'une ville etc.
    \item[-] \(O\): Les objets du votes.
    \item[-] \(u\): L' \tc{utilité} des votant.es, une fonction de des parties de \(\mathcal{O}\) dans \(\mathcal{R}\).
    \item[-] \(d\): Un vecteur de \tc{déclaration}, correspond à l'ensemble des réponse des votant.es à une question.
    \item[-] \(d_i\): La déclaration du votant \(i\)
    \item[-] \(f\): Une fonction de \tc{choix social} qui à un vecteur de déclaration associe une valeur.
    \item[-] \(\mathcal{M} = (f,\mathcal{N},Q,u,O),\mathcal{B}\): Un \tc{mécanisme} de vote est constitué d'une fonction de choix social et
        détermine le/les vainqueurs d'une élection en fonction d'un vecteur de déclaration \(d\) réponse des questions \(Q\).
\end{itemize}

\subsection{\textit{strategyproofness}}
\label{def:strategyproof}
Muni de ce cadre définissons plus formelement la notion de \textit{strategyproofness} \cite{DBLP:journals/mss/Dutta80}:

\begin{defn}[\textit{strategyproofness}] On dit d'un mécanisme \(\mathcal{M} = (f,\mathcal{N},Q,u,O),\mathcal{B}\) est \textit{strategyproof} ou \tc{truthful} ssi
    \[\forall i \in \mathcal{N},\ \forall d, \ u_i(f(d_{-i},d^*_i)) \geq u_i(f(d_{-i},d_i))\]
\end{defn}

Où $d^*_i$ correspond au bulletin du votant $i$ cohérent vis-à-vis de son utilité et $d_i$ une déclaration alternative.

Cette notion peut être comparée à celle d'équilibre de Nash, bien qu'une solution peut correspondre à un équilibre de Nash
sans que celle-ci soit strategyproof. En effet, la \textit{strategyproofness} est une notion locale et observe la déclaration d'un votant.es dès lors que tous les
autres ne mentent pas.
