\section{Sur la véracité des mécanismes binaires}
\label{sec:mat}
\subsection{Framework}
\label{sub:frame}
Par soucis de clarté on rappelle ici le cadre de l'étude de ce chapitre, les mécanismes étudiés sont:
\begin{itemize}
    \item \textit{utilitaires} (c.f. \ref{def:util})
    \item \textit{non-pondérés} \textit{i.e.} les poids des projets/objets sont de 1
    \item Toutes les solutions sont réalisables
    \item A fonction de score
    \item A bulletin \textit{binaire}
          \label{def:dom}
    \item \textit{dominant} \textit{i.e.} $\forall i,j \in [n]  \ M_{i,j} < M_{i,i}$
\end{itemize}


\subsection{Monotonie et \textit{strategyproofness}}
Le problème avec la \textit{strategyproofness} est sa difficulté d'analyse, on souhaite donc caractériser par une autre propriété
l'ensemble des mécanismes de ce cadre strategyproof. Pour cela le théorème \ref{th:ver} peut nous aider à dégager certaines caractéristiques propres
à ce cadre qui peuvent se géné\-raliser.

On définit donc une notion de \tc{monotonie} comme suit:
\begin{defn}
    On dit d'un mécanisme qu'il est \textit{monotone} si, lorsque un votant change une déclaration $d$ pour une déclaration $d'$ où $\sum_{i\in [n]} \mid d_i - d'_i \mid = 2$, on note $i$ l'objet tel que
    $d_i = 1 \land d'_i = 0$ et $j$ l'objet $d_j = 0 \land d'_j = 1$ :
    \begin{itemize}
        \item[] $i$ sort de la solution;
        \item[$\lor$] $j$ entre dans la solution;
        \item[$\lor$] la solution ne change pas;
    \end{itemize}
\end{defn}

On voit derrière cette idée de monotonie une certaine stabilité du mécanisme étudié.


\begin{prop}[Véracité et monotonie]
    Un mécanisme \textit{monotone} est strategyproof.
\end{prop}

\begin{proof}
    On se munit d'un mécanisme $\mathcal{M}$, nous supposons que $i$ déclare un ensemble d'objets pour obtenir une solution optimale $S^*$. Nous pouvons
    itérativement approcher le bulletin initial de $i$ vers un bulletin strategyproof. En effet dès lors qu'il arrête de voter pour un objet $b$ t.q $u_i(b) = 0$,
    pour un objet $b'$ t.q $u_i(b') = 1$, alors $b'$ entre dans la solution, ou $b$ en sort ou rien ne se passe \textit{i.e.} $S$ la nouvelle solution pour la déclaration
    de $i$ est similaire ou meilleure pour $i$.
\end{proof}

Le réel problème de la suite de cette étude est de l'équivalence entre la \textit{strategyproofness} et la monotonie. Pour se convaincre de la pertinence de cette étude nous pouvons déjà remarquer la chose suivante:
\begin{prop}
    Il existe des matrices de score associé à un mécanisme \textit{monotone} qui ne sont pas équivalente à \textit{participatory budgeting}.
\end{prop}

\begin{proof}
    Nous pouvons prendre comme exemple la matrice \(\begin{pNiceMatrix}
        1 & 0 \\
        0 & 2
    \end{pNiceMatrix} \) simplement puisque voter pour les deux projets n'attribue pas le même score à ces deux objets.
\end{proof}

L’intérêt de ces matrices est qu'elles pourraient décrire des sous-ensembles d'objets qui sont réalisables par un nombre moins élevés de votes que participatory
budgeting ou inversement.

\begin{exmp}
    \label{ex:serv}
    Un hébergeur souhaite mettre en service ses serveurs à des tiers. Pour cela ils leur demande de dresser une liste de serveurs préférés
    par critère de localisation, capacités de charge etc.. Par soucis d'optimisation, cet hébergeur préfère mettre à disposition des serveurs
    les plus proches les uns des autres. Pour cela il organise un vote avec une matrice modifiée, ainsi voter pour un serveur augmente la possibilité que les serveurs proches
    soient sélectionnés.
\end{exmp}

La difficulté de calculer la \textit{strategyproofness} motive cette étude. Cela dit on se heurte à la réciproque
dont aucunes des approches n'a mené à la corroborer ou la réfuter. Nous avons donc décidé de déterminer si la
monotonie ou la \textit{strategyproofness} pouvait être décrite pas un système d'inéquation.

\begin{defn}
    Soit $M$ la matrice de score d'un mécanisme $\mathcal{M}$, on notera ($\Delta$) jusqu'à la fin de cette étude le système d'inéquation suivant ($M_{j,p}$ correspond aux coefficient $(j,p)$ de la matrice $M$):
    \begin{align*}
        \forall i,j,k \  & M_{i,i} - M_{i,j} \geq M_{k,i} - M_{k,j}       \\
                         & M_{i,i} > M_{j,i} \ \land \  M_{i,i} > M_{k,i}
    \end{align*}
\end{defn}
Pour donner l'intuition derrière cet objet il décrit les matrices dont le changement de score d'un objet $k$ vers un objet $i$ augmente plus $i$ que n'augmente les autres. Nous avons donc au total $\frac{n!}{(n-3)!} = A^3_n$.
En reprenant l'exemple \ref{ex:serv} on aimerait pouvoir étant donné la matrice de l’hébergeur, renvoyer la matrice strategyproof la plus proche. Nous ne pouvons pas utiliser
l'algorithme du simplexe avec une norme $p$, on va donc faire appel à des algorithmes de programmation quadratiques.
\newline
On voudrait par exemple minimiser la norme 2 \textit{i.e.} la fonction
$$
    f_M:
    \begin{cases}
        \mathcal{M}_n(\mathds{R}) & \rightarrow \mathds{R}                                               \\
        A                         & \mapsto \sqrt{\sum_{i,j \in [n]} \left(M_{i,j} - A_{i,j} \right)^2}
    \end{cases}
$$

Nous pouvons ici simplifier $f$ en enlevant la racine, en développant et en supprimant les termes constants ce qui nous donne:
$$
    g_M:
    \begin{cases}
        \mathcal{M}_n(\mathds{R}) & \rightarrow \mathds{R}                                 \\
        A                         & \mapsto \sum_{i,j \in [n]} M_{i,j}^2 - 2A_{i,j}M_{i,j}
    \end{cases}
$$
De plus la programmation quadratique est polynomiale dans le nombre de contrainte \cite{MINE:Wolfe59} ici décrit par $\Delta$, donc
notre algorithme est polynomiale.

\subsection{$\Delta$ et la monotonie}

Maintenant que nous pouvons décrire un algorithme (c.f. Annexe \ref{code:qp}) il est nécessaire de comprendre quelles sont les matrices qu'engendre le polyèdre associé à $\Delta$

\begin{rem}
    On parle ici bien de polyèdre et non de polytope car chacune des inéquations décrit un hyperplan \textit{i.e.} un demi-espace. On regarde donc une intersection de demi-espace non bornés \textit{i.e.} un polyèdre.
\end{rem}

\subsubsection{Sur les matrices $3\times3$}

\begin{thm}[Dradilac, G.]
    \label{thm:deltaP}
    Dans le cadre des matrices de score de taille 3 sous les conditions énoncées \ref{sub:frame} , la contrainte $\Delta$ est équivalent à la monotonie.
\end{thm}

Nous avons besoin de deux lemmes supplémentaires pour la suite de cette partie.


\begin{lem}
    \setcounter{equation}{0}
    \label{lem:order}
    Étant donné un mécanisme utilitaire à $n$ objets, soit $i$, $j$, $k$ 3 objets.
    Il est possible d'obtenir les score suivant:
    \begin{align}
         & \label{eq:order1} \text{score}(i) < \text{score}(j) < \text{score}(k) \\
         & \label{eq:order2} \text{score}(j) < \text{score}(i) < \text{score}(k)
    \end{align}
\end{lem}

\begin{proof}
    Soit $M$ une matrice de score de taille $n\times n$ avec $n \geq 3$. Par hypothèse il existe un vote $v$ tel que
    $f(v) = \{k\}$. De plus dans le cas où $W = 2$ il existe une vote $v'$ tel que, \linebreak $f(v') =\{j,k\}$.
    Pour démontrer l'inégalité \ref{eq:order1}, nous supposons que dans le $v'$ nous donne $\text{score}(k) \leq \text{score}(j)$. \linebreak Or le vote $v$ nous donne:
    \begin{enumerate}
        \item $\text{score}(i) < \text{score}(j) < \text{score}(k) $
        \item $ \text{score}(j) < \text{score}(i) < \text{score}(k) $
    \end{enumerate}
    Dans le premier cas l'inégalité \ref{eq:order1} est vérifié, nous supposons donc l'inégalité \ref{eq:order2} est vérifiée. \linebreak

    On note $\delta_v = \text{score}_v(j) - \text{score}_v(k)$ et $\delta_{v'} = \text{score}_{v'}(k) - \text{score}_{v'}(j)$. On veut déterminer un bulletin $v''$ tel que:

    $$\delta_{v''} = \text{score}_{v''}(k) - \text{score}_{v''}(j) >  \left \lceil \left\lceil \frac{\text{score}_{v''}(i) - \text{score}_{v''}(j) }{ \text{score}_{v'}(j) - \text{score}_{v'}(i)} \right\rceil \times \left(\text{score}_{v}(k) - \text{score}_{v}(j) \right) \right \rceil + 1$$
    Pour cela nous pouvons simplement dilater les votes de $v$. En effet nous augmentons plus l'écart entre $k$ et $j$ qu'entre $i$ et $j$. On en conclut qu'il existe un $\lambda$ tel que $v'' = \lambda v$.
    Muni de ce vote on considère le votre $v^{(3)} = v'' +  \left\lceil \frac{\text{score}_{v''}(i) - \text{score}_{v''}(j) }{ \text{score}_{v'}(j) - \text{score}_{v'}(i)} \right\rceil \times v'$.
    Ce vote nous donne donc l'inégalité \ref{eq:order1}. Par hypothèse l'inégalité \ref{eq:order2} est vérifiée.

\end{proof}

\begin{thm}[Dradilac,G.]
    Dans le cadre des matrices de score de taille 3, $\Delta$ est équivalent à la monotonie.
\end{thm}


\begin{proof}
    \begin{itemize}
        \item[]
        \item[$\implies$] Pour démontrer cela on va étudier 3 cas:
            \begin{itemize}
                \item Si le vote sélectionne \textbf{1 objet}, nous supposons qu'un votant passe d'un objet $a$ à un objet $b$:
                \item[]
                    \begin{itemize}
                        \item Soit $a$ était dans la solution, on est dans tous les cas \textit{monotone}.
                        \item Si $b$ est dans la solution, alors
                              \begin{itemize}
                                  \item  $a$ ne peut pas entrer car notre mécanisme est dominant (c.f. \ref{def:dom}).
                                  \item  $c$ ne peut pas rentrer en vertu de la contrainte $\Delta$
                              \end{itemize}
                        \item Si $c$ est dans la solution
                              \begin{itemize}
                                  \item $a$ rentre, on aurait $M_{a,b} - M_{a,a} > M_{c,b} - M_{c,a}$
                                        qui ne respecte donc pas $\Delta$
                                  \item $b$ rentre on est \textit{monotone}.
                              \end{itemize}
                    \end{itemize}
                \item Si le vote sélectionne \textbf{2 objets} $\{a,b\}$
                \item[]
                    Nous pouvons remarquer qu'ici le cas pathologiques est si $i$ vote pour un objet $a$ ou $b$ dans la solution,
                    que $c$ l'objet pour lequel nous avons arrêté de voter rentre. Ce cas est impossible puisque l'nous augmentons plus $a$
                    ou $b$ que $c$ donc ils restent dans la solution.
                \item Si le vote sélectionne \textbf{3 objets}, il ne peut être que \textit{strategyproof}
            \end{itemize}
        \item[$\Longleftarrow$]
            Nous raisonnons par contraposée, nous supposons donc que pour l'un des objets on ait:
            \[ M_{i,i} - M_{i,j} < M_{k,i} - M_{k,j} \]
            Lorsque l'on passe d'un vote de $j$ à $i$, nous augmentons plus le score de $k$ que de $i$. On va construire une solution où en votant pour $i$ à la place de $j$, $i$ sortira et $k$ entrera.
            Par le lemme (\ref{lem:order}) nous pouvons avoir score$(i)$ > score$(k)$ > score$(j)$, si dans ce vote (que l'on appelle maintenant $v$), de plus par le lemme nous avons au moins un vote pour chaque objets.
            Nous dilatons (i.e multiplie par une constante) ce vote $v'$ de sorte à avoir au moins $\frac{M_{k,k} - M_{i,k}}{\epsilon}$ (positif par dominance) votes pour $j$. Nous pouvons maintenant
            rapprocher l'objets $i$ de l'objet $k$ en votant $\left\lfloor\frac{\text{score($i$)} - \text{score($k$)}}{M_{k,k} - M_{i,k}}\right\rfloor$ pour l'objet $k$. En effet $M_{k,k} \geq M_{k,j}$ donc $k$ se rapproche de $i$ plus rapidement que $j$.
            \newline Ainsi $i$ est à distance au plus $M_{k,k} - M_{i,k}$ de $i$, nous pouvons donc changer les votes de $j$ pour $i$ $M_{k,k} - M_{i,k}$ fois au plus. Il existe un
            vote pour lequel $k$ devient meilleure solution car l'écart se réduit de $\epsilon$ à chaque changement de vote.
    \end{itemize}
\end{proof}
\begin{prop}
    Tout mécanisme décrit dans la section \ref{sub:frame} $3\times 3$ dans $\Delta$ est \textit{strategyproof}.
\end{prop}


\begin{proof}
    Nous raisonnons par contraposé. Supposons que notre mécanisme n'est pas strategyproof, nous raisonnons par cas sur le nombre d'objet sélectionné:
    \begin{itemize}
        \item[$W = 1$:] Nous supposons que le votant $i$ en votant pour $a$ obtient $c$ tandis qu'en votant pour $c$ nous obtenons $a$ ou $b$.
            On aurait alors $M_{c,a} > M_{a,a}$ ce qui contredit la dominance \textit{i.e.} contredit $\Delta$
        \item[$W = 2$:] Nous savons que son vote non-truthful donne une utilité de 2, et un vote truthful de 1.
            Nous supposons donc que $i$ vote pour $\{a,b\}$ et obtient $\{b,c\}$; tandis qu'en votant $\{b,c\}$ il obtient
            \begin{itemize}
                \item $\{a,b\}$ nous obtiendrions :
                      $$ M_{c,c} - M_{c,a} < M_{a,c} - M_{a,a}$$ \textit{i.e.} notre mécanisme ne serait pas dominant.
                \item $\{a,c\}$ nous obtiendrions :
                      $$ M_{a,a} - M_{a,c} < M_{b,c} - M_{b,a} $$ \textit{i.e.} notre mécanisme ne respecte pas $\Delta$.
            \end{itemize}
        \item[$W = 3$:] Si un mécanisme il est truthful, la propriété est donc vérifiée.
    \end{itemize}
\end{proof}

\subsubsection{Sur les matrice $n\times n$}

Nous généralisons la dernière relation que nousavons avec tous ces objets.

\begin{prop}
    Dans le même cadre qu’énoncé section \ref{sub:frame}, tout mécanismes d'au moins 3 objets \textit{strategyproof} est dans la contrainte $\Delta$.
\end{prop}

\begin{proof}
    Nous nous donnons une matrice de score $M$, nous raisonnons par contraposée, nous supposons donc encore une fois qu'il existe trois objets $a,b,c$ tel que:
    $$  M_{b,b} - M_{b,a} < M_{c,b} - M_{c,a} $$
    Étant donné un votant $i$ on va décrire un vote non-strategyproof. En vertu du lemme (\ref{lem:order}) nous pouvons obtenir comme score:
    $$        \text{score}(a) > \text{score}(b) > \text{score}(c)
        \ \land \  \text{score}(a) > \text{score}(c) > \text{score}(b)
    $$
    Nous nommons $v_1$ et $v_2$ le bulletin réalisant ces ordres. On note $\delta_1 = (Mv_1)_b - (Mv_1)_c$ et \linebreak $\delta_2 = (Mv_1)_c - (Mv_1)_b$.
    Pour toute constante $A > 0$ il existe un bulletin tel que \linebreak $d(a,b) > A  \land d(b,c) \ \leq \ \min(\delta_1,\delta_2)$ où $d(i,j) = \mid \text{score}(i) - \text{score}(j) \mid$. Pour cela on multiplie $v_1$ par
    $\frac{A}{d(a,b)}$ on note $v_1'$ un tel bulletin. On vote maintenant $\left\lceil \frac{(Mv_1')_b - (Mv_1')_c}{(Mv_2)_c - (Mv_2)_b}\right\rceil$. Nous pouvons ensuite
    ajouter $\left\lfloor \frac{d(a,b)}{\delta_1} \right\rfloor$ $v_1$ et nous obtenons bien $d(a,b) \leq \min(\delta_1,\delta_2)$.
    Muni de ce lemme intermédiaire on choisit comme constante $\frac{(M_{a,a} - M_{a,b}) \times \min(\delta_1,\delta_2)}{\epsilon}$.
    \newline
    Nous dilatons ce nouveau bulletin par $n!$. Nous avons maintenant $d(b,c) = n! \times \min(\delta_1,\delta_2)$. Nous pouvons maintenant faire passer $b$ devant $c$
    en votant $\frac{\min(\delta_1,\delta_2)n!}{\epsilon}$ pour $b$ à la place de $a$, $c$ reste devant $a$ et $b$. Cependant puisque nous sommes dans le cadre
    d'un mécanismes à $n \geq 3$ objets, des objets peuvent être entré dans la solution et d'autre en sortir en passe de votes de $a$ pour $b$. Puisque le nombre de
    votes est divisible par $n!$ nous pouvons supposer que le votant a pu voter pour $W \in [n]$ objets. On prend $W = \text{rang}(c)$ i.e sa position dans l'ordre des scores, tout objets sorti de la solution n'étaient
    pas dans les données privées de $i$, tout objets entrant le sont. Nous avons donc un votant qui a un bulletin non-truthful strictement meilleur
    que son bulletin truthful.

\end{proof}


\begin{rem}
    On aurait pu utiliser aussi ce théorème pour la déduire la réciproque du théorème \ref{thm:deltaP} puisque ici $P \implies \Delta \implies T \implies P$
    \textit{i.e.} $\Delta \implies T$ mais on préfère diviser ces deux preuves par soucis de cohérence.
\end{rem}

Ce qui nous permet de conclure que $\Delta \iff $ monotonie $ \iff $ \textit{strategyproofness} pour les matrices $3\times 3$ donc le polyèdre décrit par
$\Delta$ est celui des matrices truthful. Pour voir le code de la programmation quadratique se référer à l'annexe \ref{code:qp}.

Cependant en élargissant le cadre d'étude, $\Delta$ est trop général pour décrire les matrices strategyproof.

\begin{prop}
    Il existe des matrices $4\times 4$ dans le polyèdre engendré par $\Delta$ mais qui ne sont pas strategyproof.
\end{prop}


\begin{proof}
    \[
        \begin{pNiceMatrix}
            4 & 0 & 0   & 0 \\
            0 & 4 & 0   & 0 \\
            0 & 0 & 4   & 0 \\
            0 & 0 & 2.5 & 3
        \end{pNiceMatrix}
    \]
    Cette matrice respecte bien $\Delta$ mais avec comme vecteur de déclaration $v = \left(4 \ 2 \ 0 \ 2 \right)^T $
    nous obtenons comme score $(16 \ 8 \ 0 \ 6)^T$ pour un votant souhaitant $(1,4)$ son bulletin optimal est $(3,4)$.
\end{proof}

Nous pouvons donc conclure par un diagramme d'Euler les résultats obtenus jusqu'ici sur les mécanismes à score \textit{monotone}s.

\vspace{1cm}

\centering

\begin{tikzpicture}
    \node [draw,
        circle,
        minimum size = 2cm] (A) at (0,0){};


    \node [draw,
        circle,
        minimum size =4cm] (B) at (0,1){};

    \node at (0,0) {$PB$};
    \node at (0,2) {$\text{Monotonie} \land \Delta \land T$};

    \node at (0,-2) {$3\times 3 \text{ score matrices}$};
    \node [draw,
        circle,
        minimum size = 2cm] (C) at (5,0){};


    \node [draw,
        circle,
        minimum size = 4cm] (D) at (5,1){};

    \node [draw,
        circle,
        minimum size = 5cm] (D) at (5,1.5){};


    \node [draw,
        circle,
        minimum size =3cm,dashed] (E) at (5,0.5){};

    \node at (5,1.5) {$\text{Monotonie}$};
    \node at (5,3.5) {$\Delta$};
    \node at (5,2.5) {$T$};
    \node at (5,0) {$PB$};

    \node at (5,-2) {$n\times n \text{ score matrices}$};


\end{tikzpicture}
